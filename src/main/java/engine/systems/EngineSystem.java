package engine.systems;

import engine.ChessEngine;

public interface EngineSystem {

    void addToEngine(int priority, ChessEngine chessEngine);

    void update();
}
