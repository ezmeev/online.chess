package server.message.services;

import server.message.models.Message;
import server.message.models.MessageType;
import server.message.utils.OpCode;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Random;

public class MessageWriter {

    private OutputStream output;

    public MessageWriter(OutputStream output) {
        this.output = output;
    }

    public void writeMessage(Message message) {
        byte[] data = createFrame(toOpCode(message.getType()), message.getPayload());
        try {
            output.write(data);
            output.flush();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private OpCode toOpCode(MessageType type) {
        switch (type) {
            case Closing:
                return OpCode.CLOSING;
            case Text:
                return OpCode.TEXT;
            case Ping:
                return OpCode.PING;
            case Pong:
                return OpCode.PONG;
            case Binary:
                return OpCode.BINARY;
            case Continuous:
                return OpCode.CONTINUOUS;
            default:
                throw new RuntimeException("Unknown message type:" + type.name());
        }
    }

    private byte[] createFrame(OpCode code, String payloadString) {
        byte[] payload = payloadString.getBytes(StandardCharsets.UTF_8);

        boolean mask = false;
        int sizeInBytes;
        if (payload.length <= 125) {
            sizeInBytes = 1;
        } else if (payload.length <= 65535) {
            sizeInBytes = 2;
        } else {
            sizeInBytes = 8;
        }

        int i1 = sizeInBytes > 1 ? sizeInBytes + 1 : sizeInBytes;
        int i2 = mask ? 4 + 1 : 0;

        ByteBuffer buf = ByteBuffer.allocate(1 + i1 + i2 + payload.length);

        byte one = (byte) -128;
        one = (byte) (one | (byte) code.code);
        buf.put(one);

        byte[] payloadlengthbytes = toByteArray(payload.length, sizeInBytes);

        assert (payloadlengthbytes.length == sizeInBytes);

        if (sizeInBytes == 1) {
            buf.put((byte) (payloadlengthbytes[0] | (mask ? (byte) -128 : 0)));
        } else if (sizeInBytes == 2) {
            buf.put((byte) (126 | (mask ? -128 : 0)));
            buf.put(payloadlengthbytes);
        } else if (sizeInBytes == 8) {
            buf.put((byte) (127 | (mask ? -128 : 0)));
            buf.put(payloadlengthbytes);
        } else {
            throw new RuntimeException("Size representation not supported/specified");
        }

        if (mask) {
            ByteBuffer maskKey = ByteBuffer.allocate(4);
            maskKey.putInt(new Random().nextInt());
            buf.put(maskKey.array());
            for (int i : payload) {
                buf.put((byte) (payload[i] ^ maskKey.get(i % 4)));
            }
        } else {
            buf.put(payload);
        }

        buf.flip();

        return buf.array();
    }

    private byte[] toByteArray(int value, int byteCount) {
        byte[] buffer = new byte[byteCount];
        int highest = 8 * byteCount - 8;
        for (int i = 0; i < byteCount; i++) {
            buffer[i] = (byte) (value >> (highest - 8 * i));
        }
        return buffer;
    }
}