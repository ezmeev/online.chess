package board.figures;

import board.Board;
import board.BoardPosition;
import board.FigureMove;
import board.Side;

import java.util.ArrayList;
import java.util.List;

public class King extends Figure {

    public King(int idx, Side side, BoardPosition position) {
        super(idx, side, position);
    }

    @Override
    public List<FigureMove> getPossibleMoves(Board board) {

        BoardPosition position = getPosition();

        List<FigureMove> moves = new ArrayList<>();

        tryAddConsideringAttacks(board, moves, position, 1, 1);
        tryAddConsideringAttacks(board, moves, position, -1, 1);
        tryAddConsideringAttacks(board, moves, position, 1, -1);
        tryAddConsideringAttacks(board, moves, position, -1, -1);

        tryAddConsideringAttacks(board, moves, position, 1, 0);
        tryAddConsideringAttacks(board, moves, position, -1, 0);
        tryAddConsideringAttacks(board, moves, position, 0, 1);
        tryAddConsideringAttacks(board, moves, position, 0, -1);

        return moves;
    }

    @Override
    public List<FigureMove> getPossibleAttackMoves(Board board) {
        BoardPosition position = getPosition();

        List<FigureMove> moves = new ArrayList<>();

        tryAdd(board, moves, position, 1, 1);
        tryAdd(board, moves, position, -1, 1);
        tryAdd(board, moves, position, 1, -1);
        tryAdd(board, moves, position, -1, -1);

        tryAdd(board, moves, position, 1, 0);
        tryAdd(board, moves, position, -1, 0);
        tryAdd(board, moves, position, 0, 1);
        tryAdd(board, moves, position, 0, -1);

        return moves;
    }

    protected void tryAddConsideringAttacks(Board board, List<FigureMove> moves, BoardPosition position, int addRow,
            int addCol) {
        if (position.isOnBoard(addRow, addCol)) {
            boolean tileIsEmpty = emptyTile(board, position.addRowCol(addRow, addCol));
            boolean tileIsEnemy = enemyTile(board, position.addRowCol(addRow, addCol));
            boolean tileIsAttacked = attackedTile(board, position.addRowCol(addRow, addCol));
            if (!tileIsAttacked && (tileIsEmpty || tileIsEnemy)) {
                moves.add(new FigureMove(position, position.addRowCol(addRow, addCol), tileIsEnemy));
            }
        }
    }

    private boolean attackedTile(Board board, BoardPosition boardPosition) {
        return board.isPositionAttackedBySide(getSide().opposite(), boardPosition);
    }
}
