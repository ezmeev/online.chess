package server;

import json.JsonHelpers;
import json.models.GameStateModel;
import server.message.models.TextMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ChessPlayers {

    private List<PlayerSocketHandler> playerSocketHandlerList = Collections.synchronizedList(new ArrayList<>());

    public void add(PlayerSocketHandler playerHandler) {
        playerSocketHandlerList.add(playerHandler);
    }

    public void sendGameStateUpdate(GameStateModel gameStateModel) {
        for (PlayerSocketHandler handler : playerSocketHandlerList) {
            handler.send(new TextMessage(JsonHelpers.toJson(gameStateModel)));
        }
    }

    public String getSnapshot() {
        return playerSocketHandlerList
                .stream()
                .map(PlayerSocketHandler::getSessionId)
                .collect(Collectors.joining());
    }
}
