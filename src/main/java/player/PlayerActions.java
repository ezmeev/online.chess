package player;

public enum PlayerActions {
    MoveFigure,
    SelectFigure,
    Reset,
    Disconnect,
    Join
}
