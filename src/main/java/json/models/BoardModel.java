package json.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BoardModel {

    @JsonProperty("tiles")
    public TileModel[][] tiles = new TileModel[8][8];
}
