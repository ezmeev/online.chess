package json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonHelpers {

    private static final ObjectMapper jsonSerializer = new ObjectMapper();

    private JsonHelpers() {
    }

    public static String toJson(Object o) {
        try {
            return jsonSerializer.writeValueAsString(o);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T fromJson(String json, Class<T> cls) {
        try {
            return jsonSerializer.readValue(json, cls);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
