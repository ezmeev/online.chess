package engine.systems;

import board.GameState;
import engine.ChessEngine;

public class GameStateSystem implements EngineSystem {

    private GameState state;

    public GameStateSystem(GameState state) {
        this.state = state;
    }

    @Override
    public void addToEngine(int priority, ChessEngine chessEngine) {
        state.init();
        chessEngine.addSystem(priority, this);
    }

    @Override
    public void update() {

    }

    public GameState getState() {
        return state;
    }
}
