import board.GameState;
import engine.ChessEngine;
import engine.systems.ClientSynchronizationSystem;
import engine.systems.GameStateSystem;
import engine.systems.PlayersSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import player.PlayerActionModel;
import server.ChessPlayers;
import server.ChessServer;

import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;

public class EntryPoint {

    public static final Logger LOGGER = LoggerFactory.getLogger(EntryPoint.class);

    public static void main(String[] args) {

        GameState gameState = new GameState();

        ChessEngine chessEngine = new ChessEngine();

        ChessPlayers players = new ChessPlayers();
        Queue<PlayerActionModel> playerActions = new ConcurrentLinkedQueue<>();
        ChessServer chessServer = new ChessServer(players, playerActions);

        GameStateSystem gameStateSystem = new GameStateSystem(gameState);
        gameStateSystem.addToEngine(1, chessEngine);

        PlayersSystem playersSystem = new PlayersSystem(players, playerActions);
        playersSystem.addToEngine(2, chessEngine);

        ClientSynchronizationSystem clientSynchronizationSystem = new ClientSynchronizationSystem();
        clientSynchronizationSystem.addToEngine(3, chessEngine);

        CompletableFuture<?> engineFuture = CompletableFuture.runAsync(chessEngine::start).exceptionally(e -> {
            throw new RuntimeException("Unable to start engine:", e);
        });
        CompletableFuture<?> serverFuture = CompletableFuture.runAsync(chessServer::start).exceptionally(e -> {
            throw new RuntimeException("Unable to start server:", e);
        });

        CompletableFuture.anyOf(engineFuture, serverFuture).exceptionally(e -> {
            LOGGER.error("Unable to start:", e);
            return null;
        }).join();
    }
}
