package server.message.models;

public class DeadMessage implements Message {

    public String getPayload() {
        return "";
    }

    public MessageType getType() {
        return MessageType.Closing;
    }
}