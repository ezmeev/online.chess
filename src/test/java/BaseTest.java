import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import player.PlayerActionModel;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;

public class BaseTest {

    @Test
    public void test1() throws IOException {

        ObjectMapper m = new ObjectMapper();

        String json = "{\"action\":\"SelectFigure\",\"payload\":{\"figureIdx\":10}}";
        JsonNode model = m.readValue(json, JsonNode.class);

        System.out.println(model.get("action").asText());
        System.out.println(model.get("payload").toString());

        assertNotNull(model);
    }
}
