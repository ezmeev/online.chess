package json.models;

import board.Side;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GameStateModel {

    @JsonProperty("board")
    public BoardModel board;

    @JsonProperty("sideToMove")
    public Side sideToMove;

    @JsonProperty("whiteSideSessionId")
    public String whiteSideSessionId;

    @JsonProperty("blackSideSessionId")
    public String blackSideSessionId;

    @JsonProperty("selectedFigure")
    public FigureModel selectedFigure;
}
