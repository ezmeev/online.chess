package player.actions;

import board.BoardPosition;
import json.models.MoveFigureActionModel;

public class MoveFigureAction {

    public int figureIdx;
    public BoardPosition fromPosition;
    public BoardPosition toPosition;

    public MoveFigureAction(MoveFigureActionModel actionModel) {
        figureIdx = actionModel.figureIdx;
        fromPosition = new BoardPosition(actionModel.fromPosition);
        toPosition = new BoardPosition(actionModel.toPosition);
    }
}
