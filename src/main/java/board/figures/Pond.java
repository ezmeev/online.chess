package board.figures;

import board.Board;
import board.BoardPosition;
import board.FigureMove;
import board.Side;

import java.util.ArrayList;
import java.util.List;

public class Pond extends Figure {

    public Pond(int idx, Side side, BoardPosition position) {
        super(idx, side, position);
    }

    @Override
    public List<FigureMove> getPossibleMoves(Board board) {
        BoardPosition position = getPosition();

        List<FigureMove> moves = new ArrayList<>();

        if (getSide() == Side.WHITE) {

            if (position.isOnBoard(1, 0) && emptyTile(board, position.addRow(1))) {
                moves.add(new FigureMove(position, position.addRow(1)));
            }
            if (position.getRow() == 1 && emptyTile(board, position.addRow(1)) && emptyTile(board, position.addRow(2))) {
                moves.add(new FigureMove(position, position.addRow(2)));
            }
            if (position.isOnBoard(1, -1) && enemyTile(board, position.addRowCol(1, -1))) {
                moves.add(new FigureMove(position, position.addRowCol(1, -1), true));
            }
            if (position.isOnBoard(1, 1) && enemyTile(board, position.addRowCol(1, 1))) {
                moves.add(new FigureMove(position, position.addRowCol(1, 1), true));
            }

        } else {

            if (position.isOnBoard(-1, 0) && emptyTile(board, position.addRow(-1))) {
                moves.add(new FigureMove(position, position.addRow(-1)));
            }
            if (position.getRow() == 6 && emptyTile(board, position.addRow(-1)) && emptyTile(board, position.addRow(-2))) {
                moves.add(new FigureMove(position, position.addRow(-2)));
            }
            if (position.isOnBoard(-1, -1) && enemyTile(board, position.addRowCol(-1, -1))) {
                moves.add(new FigureMove(position, position.addRowCol(-1, -1), true));
            }
            if (position.isOnBoard(-1, 1) && enemyTile(board, position.addRowCol(-1, 1))) {
                moves.add(new FigureMove(position, position.addRowCol(-1, 1), true));
            }
        }

        return moves;
    }

    @Override
    public List<FigureMove> getPossibleAttackMoves(Board board) {
        BoardPosition position = getPosition();
        List<FigureMove> moves = new ArrayList<>();

        if (getSide() == Side.WHITE) {

            if (position.isOnBoard(1, -1)) {
                moves.add(new FigureMove(position, position.addRowCol(1, -1), true));
            }
            if (position.isOnBoard(1, 1)) {
                moves.add(new FigureMove(position, position.addRowCol(1, 1), true));
            }

        } else {

            if (position.isOnBoard(-1, -1)) {
                moves.add(new FigureMove(position, position.addRowCol(-1, -1), true));
            }
            if (position.isOnBoard(-1, 1)) {
                moves.add(new FigureMove(position, position.addRowCol(-1, 1), true));
            }
        }
        return moves;
    }
}
