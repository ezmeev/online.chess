package server.message.models;

public class PingMessage implements Message {

    private String payload;

    public PingMessage(String payload) {
        this.payload = payload;
    }

    public String getPayload() {
        return payload;
    }

    public MessageType getType() {
        return MessageType.Ping;
    }
}