package engine.systems;

import board.GameState;
import board.Side;
import engine.ChessEngine;
import json.JsonHelpers;
import json.models.MoveFigureActionModel;
import json.models.SelectFigureActionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import player.PlayerActionModel;
import player.PlayerActions;
import player.actions.MoveFigureAction;
import player.actions.SelectFigureAction;
import server.ChessPlayers;

import java.util.Queue;

public class PlayersSystem implements EngineSystem {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayersSystem.class);

    private ChessPlayers players;
    private Queue<PlayerActionModel> playerActions;
    private GameStateSystem gameStateSystem;

    public PlayersSystem(ChessPlayers players, Queue<PlayerActionModel> playerActions) {
        this.players = players;
        this.playerActions = playerActions;
    }

    @Override
    public void addToEngine(int priority, ChessEngine chessEngine) {
        gameStateSystem = chessEngine.getSystem(GameStateSystem.class);

        chessEngine.addSystem(priority, this);
    }

    @Override
    public void update() {
        PlayerActionModel playerAction = playerActions.poll();

        /** TODO validate:
         * here:
         *  1. player allowed to make this action
         *      + correct playerId to action
         *
         * in game state:
         *  1. action is valid for current game state
         *      + correct side to move
         *      + 'to' tile accessible for given figure
         *  2. game state is valid after action
         *      + moved side king is not under check
         *      + moved figure still on board
         */

        if (playerAction != null) {
            GameState gameState = gameStateSystem.getState();
            PlayerActions actionType = playerAction.action;

            if (!isValidActionForPlayer(actionType, playerAction.playerId, gameState)) {
                return;
            }

            switch (actionType) {
                case MoveFigure:
                    moveFigure(playerAction, gameState);
                    break;
                case SelectFigure:
                    selectFigure(playerAction, gameState);
                    break;
                case Join:
                    assignPlayerToSide(playerAction, gameState);
                    break;
                case Disconnect:
                    updateSides(playerAction, gameState);
                    break;
                case Reset:
                    resetGame(playerAction, gameState);
                    break;
            }
        }
    }

    private void updateSides(PlayerActionModel playerAction, GameState gameState) {
        if (gameState.getWhiteSideSessionId().equals(playerAction.playerId)) {
            gameState.setWhiteSideSessionId(null);
        } else if (gameState.getBlackSideSessionId().equals(playerAction.playerId)) {
            gameState.setBlackSideSessionId(null);
        }
    }

    private boolean isValidActionForPlayer(PlayerActions actionType, String playerId, GameState gameState) {
//        if (actionType == PlayerActions.Join || true) { // FIXME this only for dev
        if (actionType == PlayerActions.Join || actionType == PlayerActions.Disconnect) {
            return true;
        }
        return gameState.getSideToMove() == Side.WHITE && playerId.equals(gameState.getWhiteSideSessionId())
                || gameState.getSideToMove() == Side.BLACK && playerId.equals(gameState.getBlackSideSessionId());
    }

    private void resetGame(PlayerActionModel playerAction, GameState gameState) {
        gameState.init();
    }

    private void assignPlayerToSide(PlayerActionModel playerAction, GameState gameState) {
        if (gameState.getWhiteSideSessionId() == null) {
            gameState.setWhiteSideSessionId(playerAction.payload);
        } else if (gameState.getBlackSideSessionId() == null) {
            gameState.setBlackSideSessionId(playerAction.payload);
        }
    }

    private void selectFigure(PlayerActionModel playerAction, GameState gameState) {
        SelectFigureActionModel actionModel = JsonHelpers.fromJson(playerAction.payload, SelectFigureActionModel.class);
        gameState.selectFigure(new SelectFigureAction(actionModel));
    }

    private void moveFigure(PlayerActionModel playerAction, GameState gameState) {
        MoveFigureActionModel actionModel = JsonHelpers.fromJson(playerAction.payload, MoveFigureActionModel.class);
        gameState.moveFigure(new MoveFigureAction(actionModel));
    }

    public ChessPlayers getPlayers() {
        return players;
    }
}
