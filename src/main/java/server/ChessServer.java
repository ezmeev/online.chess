package server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import player.PlayerActionModel;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChessServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChessServer.class);
    private static final int PORT = 8081;

    private final ExecutorService socketExecutorService;
    private Queue<PlayerActionModel> playerActions;
    private ChessPlayers players;

    public ChessServer(ChessPlayers players, Queue<PlayerActionModel> playerActions) {
        this.socketExecutorService = Executors.newFixedThreadPool(10);
        this.playerActions = playerActions;
        this.players = players;
    }

    public void start() {
        LOGGER.info("Starting server ...");
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            while (true) {
                Socket socket = serverSocket.accept();
                LOGGER.info("New player connected");
                socketExecutorService.execute(() -> players.add(createPlayerHandler(socket)));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private PlayerSocketHandler createPlayerHandler(Socket socket) {
        try {
            return new PlayerSocketHandler(socket, this.playerActions);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

