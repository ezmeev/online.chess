package board;

import board.figures.Figure;
import board.figures.King;
import com.fasterxml.jackson.annotation.JsonProperty;
import json.JsonHelpers;
import json.models.GameStateModel;
import player.actions.MoveFigureAction;
import player.actions.SelectFigureAction;

public class GameState {

    @JsonProperty("selectedFigure")
    private Figure selectedFigure = null;

    @JsonProperty("sideToMove")
    private Side sideToMove = Side.WHITE;

    @JsonProperty("whiteSideSessionId")
    private String whiteSideSessionId;

    @JsonProperty("blackSideSessionId")
    private String blackSideSessionId;

    @JsonProperty("board")
    private Board board;

    public GameState() {
        init();
    }

    public void init() {
        board = new Board();
        sideToMove = Side.WHITE;
    }

    public void selectFigure(SelectFigureAction selectFigureAction) {
        BoardHelpers.removeHighlights(board);

        BoardPosition position = selectFigureAction.position;
        Figure figure = board.getFigure(position);

        if (figure.getSide() == sideToMove) {
            if (selectedFigure == null || selectedFigure.getIdx() != figure.getIdx()) {
                selectedFigure = figure;

                BoardHelpers.highlightAllowedMoves(board, selectedFigure);
                BoardHelpers.highlightAttacked(board, selectedFigure);
            } else {
                selectedFigure = null;
            }
        }

        BoardHelpers.highlightCheckIfAny(board);
    }

    public void moveFigure(MoveFigureAction moveFigureAction) {
        BoardPosition from = moveFigureAction.fromPosition;
        BoardPosition to = moveFigureAction.toPosition;

        Figure figure = board.getFigure(from);
        FigureMove move = new FigureMove(from, to);

        if (!figure.isMoveAllowed(board, move) || figure.getSide() != sideToMove) {
            return;
        }

        MoveCommand command = new MoveCommand(board.getFigure(move.getFrom()), board.getFigure(move.getTo()), move);

        board.applyMoveCommand(command);

        if (isValidStateFor(sideToMove, board)) {
            selectedFigure = null;
            sideToMove = figure.getSide().opposite();
            BoardHelpers.removeHighlights(board);
        } else {
            board.undoMoveCommand(command);
        }
        BoardHelpers.highlightCheckIfAny(board);
    }

    private boolean isValidStateFor(Side sideToMove, Board board) {
        King king = board.findFirstFigureOfType(sideToMove, King.class);
        return !board.isPositionAttackedBySide(sideToMove.opposite(), king.getPosition());
    }

    public Figure getSelectedFigure() {
        return selectedFigure;
    }

    public Side getSideToMove() {
        return sideToMove;
    }

    public Board getBoard() {
        return board;
    }

    public String getWhiteSideSessionId() {
        return whiteSideSessionId;
    }

    public void setWhiteSideSessionId(String whiteSideSessionId) {
        this.whiteSideSessionId = whiteSideSessionId;
    }

    public String getBlackSideSessionId() {
        return blackSideSessionId;
    }

    public void setBlackSideSessionId(String blackSideSessionId) {
        this.blackSideSessionId = blackSideSessionId;
    }

    public GameStateModel toModel() {
        GameStateModel model = new GameStateModel();
        model.board = getBoard().toModel();
        model.selectedFigure = getSelectedFigure() == null ? null : getSelectedFigure().toModel();
        model.whiteSideSessionId = getWhiteSideSessionId();
        model.blackSideSessionId = getBlackSideSessionId();
        model.sideToMove = getSideToMove();
        return model;
    }

    public String getSnapshot() {
        GameStateModel gameStateModel = toModel();
        return JsonHelpers.toJson(gameStateModel.board.tiles) + ";" + JsonHelpers.toJson(gameStateModel.selectedFigure);
    }
}
