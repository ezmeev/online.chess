package board;

import board.figures.*;

import java.util.List;
import java.util.stream.Collectors;

public class BoardHelpers {

    private BoardHelpers() {

    }

    public static BoardTile[][] setupTiles() {
        int idx = 0;

        BoardTile[][] tiles = new BoardTile[8][8];
        tiles[0][0] = new BoardTile(new Rood(idx++, Side.WHITE, new BoardPosition(0, 0)), Side.BLACK);
        tiles[0][1] = new BoardTile(new Horseman(idx++, Side.WHITE, new BoardPosition(0, 1)), Side.WHITE);
        tiles[0][2] = new BoardTile(new Minister(idx++, Side.WHITE, new BoardPosition(0, 2)), Side.BLACK);
        tiles[0][3] = new BoardTile(new Queen(idx++, Side.WHITE, new BoardPosition(0, 3)), Side.WHITE);
        tiles[0][4] = new BoardTile(new King(idx++, Side.WHITE, new BoardPosition(0, 4)), Side.BLACK);
        tiles[0][5] = new BoardTile(new Minister(idx++, Side.WHITE, new BoardPosition(0, 5)), Side.WHITE);
        tiles[0][6] = new BoardTile(new Horseman(idx++, Side.WHITE, new BoardPosition(0, 6)), Side.BLACK);
        tiles[0][7] = new BoardTile(new Rood(idx++, Side.WHITE, new BoardPosition(0, 7)), Side.WHITE);

        tiles[1][0] = new BoardTile(new Pond(idx++, Side.WHITE, new BoardPosition(1, 0)), Side.WHITE);
        tiles[1][1] = new BoardTile(new Pond(idx++, Side.WHITE, new BoardPosition(1, 1)), Side.BLACK);
        tiles[1][2] = new BoardTile(new Pond(idx++, Side.WHITE, new BoardPosition(1, 2)), Side.WHITE);
        tiles[1][3] = new BoardTile(new Pond(idx++, Side.WHITE, new BoardPosition(1, 3)), Side.BLACK);
        tiles[1][4] = new BoardTile(new Pond(idx++, Side.WHITE, new BoardPosition(1, 4)), Side.WHITE);
        tiles[1][5] = new BoardTile(new Pond(idx++, Side.WHITE, new BoardPosition(1, 5)), Side.BLACK);
        tiles[1][6] = new BoardTile(new Pond(idx++, Side.WHITE, new BoardPosition(1, 6)), Side.WHITE);
        tiles[1][7] = new BoardTile(new Pond(idx++, Side.WHITE, new BoardPosition(1, 7)), Side.BLACK);

        tiles[2][0] = new BoardTile(null, Side.BLACK);
        tiles[2][1] = new BoardTile(null, Side.WHITE);
        tiles[2][2] = new BoardTile(null, Side.BLACK);
        tiles[2][3] = new BoardTile(null, Side.WHITE);
        tiles[2][4] = new BoardTile(null, Side.BLACK);
        tiles[2][5] = new BoardTile(null, Side.WHITE);
        tiles[2][6] = new BoardTile(null, Side.BLACK);
        tiles[2][7] = new BoardTile(null, Side.WHITE);

        tiles[3][0] = new BoardTile(null, Side.WHITE);
        tiles[3][1] = new BoardTile(null, Side.BLACK);
        tiles[3][2] = new BoardTile(null, Side.WHITE);
        tiles[3][3] = new BoardTile(null, Side.BLACK);
        tiles[3][4] = new BoardTile(null, Side.WHITE);
        tiles[3][5] = new BoardTile(null, Side.BLACK);
        tiles[3][6] = new BoardTile(null, Side.WHITE);
        tiles[3][7] = new BoardTile(null, Side.BLACK);

        tiles[4][0] = new BoardTile(null, Side.BLACK);
        tiles[4][1] = new BoardTile(null, Side.WHITE);
        tiles[4][2] = new BoardTile(null, Side.BLACK);
        tiles[4][3] = new BoardTile(null, Side.WHITE);
        tiles[4][4] = new BoardTile(null, Side.BLACK);
        tiles[4][5] = new BoardTile(null, Side.WHITE);
        tiles[4][6] = new BoardTile(null, Side.BLACK);
        tiles[4][7] = new BoardTile(null, Side.WHITE);

        tiles[5][0] = new BoardTile(null, Side.WHITE);
        tiles[5][1] = new BoardTile(null, Side.BLACK);
        tiles[5][2] = new BoardTile(null, Side.WHITE);
        tiles[5][3] = new BoardTile(null, Side.BLACK);
        tiles[5][4] = new BoardTile(null, Side.WHITE);
        tiles[5][5] = new BoardTile(null, Side.BLACK);
        tiles[5][6] = new BoardTile(null, Side.WHITE);
        tiles[5][7] = new BoardTile(null, Side.BLACK);

        tiles[6][0] = new BoardTile(new Pond(idx++, Side.BLACK, new BoardPosition(6, 0)), Side.BLACK);
        tiles[6][1] = new BoardTile(new Pond(idx++, Side.BLACK, new BoardPosition(6, 1)), Side.WHITE);
        tiles[6][2] = new BoardTile(new Pond(idx++, Side.BLACK, new BoardPosition(6, 2)), Side.BLACK);
        tiles[6][3] = new BoardTile(new Pond(idx++, Side.BLACK, new BoardPosition(6, 3)), Side.WHITE);
        tiles[6][4] = new BoardTile(new Pond(idx++, Side.BLACK, new BoardPosition(6, 4)), Side.BLACK);
        tiles[6][5] = new BoardTile(new Pond(idx++, Side.BLACK, new BoardPosition(6, 5)), Side.WHITE);
        tiles[6][6] = new BoardTile(new Pond(idx++, Side.BLACK, new BoardPosition(6, 6)), Side.BLACK);
        tiles[6][7] = new BoardTile(new Pond(idx++, Side.BLACK, new BoardPosition(6, 7)), Side.WHITE);

        tiles[7][0] = new BoardTile(new Rood(idx++, Side.BLACK, new BoardPosition(7, 0)), Side.WHITE);
        tiles[7][1] = new BoardTile(new Horseman(idx++, Side.BLACK, new BoardPosition(7, 1)), Side.BLACK);
        tiles[7][2] = new BoardTile(new Minister(idx++, Side.BLACK, new BoardPosition(7, 2)), Side.WHITE);
        tiles[7][3] = new BoardTile(new Queen(idx++, Side.BLACK, new BoardPosition(7, 3)), Side.BLACK);
        tiles[7][4] = new BoardTile(new King(idx++, Side.BLACK, new BoardPosition(7, 4)), Side.WHITE);
        tiles[7][5] = new BoardTile(new Minister(idx++, Side.BLACK, new BoardPosition(7, 5)), Side.BLACK);
        tiles[7][6] = new BoardTile(new Horseman(idx++, Side.BLACK, new BoardPosition(7, 6)), Side.WHITE);
        tiles[7][7] = new BoardTile(new Rood(idx, Side.BLACK, new BoardPosition(7, 7)), Side.BLACK);

        return tiles;
    }

    public static void highlightAllowedMoves(Board board, Figure selectedFigure) {
        BoardTile[][] tiles = board.getTiles();

        List<FigureMove> possibleMoves = selectedFigure.getPossibleMoves(board);

        for (FigureMove move : possibleMoves) {
            tiles[move.getTo().getRow()][move.getTo().getCol()].setAvailableForMove(true);
        }
    }

    public static void highlightAttacked(Board board, Figure selectedFigure) {
        BoardTile[][] tiles = board.getTiles();

        List<FigureMove> attackMoves = selectedFigure.getPossibleMoves(board)
                .stream()
                .filter(m -> m.isAttack())
                .collect(Collectors.toList());

        for (FigureMove move : attackMoves) {
            tiles[move.getTo().getRow()][move.getTo().getCol()].setUnderAttack(true);
        }
    }

    public static void removeHighlights(Board board) {
        for (BoardTile[] row : board.getTiles()) {
            for (BoardTile tile : row) {
                tile.setAvailableForMove(false);
                tile.setUnderAttack(false);
            }
        }
    }

    public static void highlightCheckIfAny(Board board) {
        List<King> kings = board.findFiguresOfType(King.class);

        for (King king : kings) {
            BoardPosition kingPosition = king.getPosition();
            if (board.isPositionAttackedBySide(king.getSide().opposite(), kingPosition)) {
                board.getTiles()[kingPosition.getRow()][kingPosition.getCol()].setUnderAttack(true);
            }
        }
    }
}
