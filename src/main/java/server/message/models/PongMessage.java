package server.message.models;

public class PongMessage implements Message {

    public String getPayload() {
        return "";
    }

    public MessageType getType() {
        return MessageType.Pong;
    }
}