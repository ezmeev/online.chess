package server.message.services;

import server.message.models.DeadMessage;
import server.message.models.Message;
import server.message.models.PingMessage;
import server.message.models.TextMessage;
import server.message.utils.ByteUtils;
import server.message.utils.OpCode;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

public class MessageReader {

    private InputStream input;

    public MessageReader(InputStream input) throws IOException {
        this.input = input;
    }

    public Message readMessage() throws IOException {
        int b1 = input.read();
        if (b1 == -1) {
            return new DeadMessage();
        }

        OpCode opCode = ByteUtils.toOpCode((byte) b1);

        switch (opCode) {
            case TEXT:
                return new TextMessage(readPayload());
            case PING:
                return new PingMessage(readPayload());
            case CLOSING:
                return new DeadMessage();
            case BINARY:
            case PONG:
            case CONTINUOUS:
            default:
                throw new IllegalStateException("Unsupported OpCode: " + opCode);
        }
    }

    private String readPayload() throws IOException {
        byte b2 = (byte) input.read();
        int length = getLength(b2);
        byte[] maskKey = getMaskKey(b2);
        return new String(readPayload(input, maskKey, length), StandardCharsets.UTF_8);
    }

    private Integer getLength(Byte b2) throws IOException {
        int length = ByteUtils.toLength(b2);
        if (length <= 125) {
            return length;
        }

        if (length == 126) {
            byte[] buffer = new byte[3];
            buffer[1] = (byte) input.read();
            buffer[2] = (byte) input.read();
            return new BigInteger(buffer).intValueExact();
        }

        if (length == 127) {
            byte[] buffer = new byte[8];
            input.read(buffer);
            return new BigInteger(buffer).intValueExact();
        }
        throw new RuntimeException("Illegal length: $length");
    }

    private byte[] getMaskKey(byte b2) throws IOException {
        byte[] maskKey = null;
        if (ByteUtils.masked(b2)) {
            maskKey = new byte[4];
            input.read(maskKey);
        }
        return maskKey;
    }

    private byte[] readPayload(InputStream inStream, byte[] maskKey, int payloadLength) throws IOException {
        byte[] data = new byte[payloadLength];

        if (inStream.read(data) != data.length) {
            throw new IllegalStateException("Didn't read all the bytes.");
        }

        if (maskKey != null) {
            for (int i = 0; i < payloadLength; i++){
                data[i] = (byte) (data[i] ^ maskKey[i % 4]);
            }
        }
        return data;
    }
}