package server.message.models;

public interface Message {
    MessageType getType();
    String getPayload();
}