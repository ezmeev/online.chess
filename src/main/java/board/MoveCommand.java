package board;

import board.figures.Figure;

public class MoveCommand {

    private Figure figureToMove;

    private Figure figureToDie;

    private BoardPosition figureToMovePosition;

    private BoardPosition figureToDiePosition;

    public MoveCommand(Figure figureToMove, Figure figureToDie, FigureMove move) {
        this.figureToMove = figureToMove;
        this.figureToDie = figureToDie;
        this.figureToMovePosition = move.getFrom();
        this.figureToDiePosition = move.getTo();
    }

    public Figure getFigureToMove() {
        return figureToMove;
    }

    public Figure getFigureToDie() {
        return figureToDie;
    }

    public BoardPosition getFigureToMovePosition() {
        return figureToMovePosition;
    }

    public BoardPosition getFigureToDiePosition() {
        return figureToDiePosition;
    }
}

