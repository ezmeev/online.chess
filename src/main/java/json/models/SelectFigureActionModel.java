package json.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SelectFigureActionModel {
    @JsonProperty
    public int figureIdx;

    @JsonProperty
    public BoardPositionModel position;
}
