package server.message.models;

public class TextMessage implements Message {

    private String payload;

    public TextMessage(String payload) {

        this.payload = payload;
    }

    public String getPayload() {
        return payload;
    }

    public MessageType getType() {
        return MessageType.Text;
    }
}