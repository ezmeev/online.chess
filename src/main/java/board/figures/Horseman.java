package board.figures;

import board.Board;
import board.BoardPosition;
import board.FigureMove;
import board.Side;

import java.util.ArrayList;
import java.util.List;

public class Horseman extends Figure {

    public Horseman(int idx, Side side, BoardPosition position) {
        super(idx, side, position);
    }

    @Override
    public List<FigureMove> getPossibleMoves(Board board) {
        BoardPosition position = getPosition();

        List<FigureMove> moves = new ArrayList<>();

        tryAdd(board, moves, position, 2, 1);
        tryAdd(board, moves, position, -2, 1);

        tryAdd(board, moves, position, 2, -1);
        tryAdd(board, moves, position, -2, -1);

        tryAdd(board, moves, position, 1, 2);
        tryAdd(board, moves, position, -1, 2);

        tryAdd(board, moves, position, 1, -2);
        tryAdd(board, moves, position, -1, -2);

        return moves;
    }

    @Override
    public List<FigureMove> getPossibleAttackMoves(Board board) {
        return getPossibleMoves(board);
    }
}
