package board;

public class FigureMove {

    private boolean attack = false;
    private BoardPosition from;
    private BoardPosition to;

    public FigureMove(BoardPosition from, BoardPosition to) {
        this(from, to, false);
    }

    public FigureMove(BoardPosition from, BoardPosition to, boolean isAttack) {
        this.from = from;
        this.to = to;
        this.attack = isAttack;
    }

    public BoardPosition getFrom() {
        return from;
    }

    public BoardPosition getTo() {
        return to;
    }

    public boolean isAttack() {
        return attack;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FigureMove that = (FigureMove) o;

        if (!getFrom().equals(that.getFrom())) {
            return false;
        }
        return getTo().equals(that.getTo());
    }

    @Override
    public int hashCode() {
        int result = getFrom().hashCode();
        result = 31 * result + getTo().hashCode();
        return result;
    }
}
