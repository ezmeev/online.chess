package board;

import json.models.BoardPositionModel;

public class BoardPosition {

    private int row;
    private int col;

    public BoardPosition(BoardPositionModel position) {
        this(position.row, position.col);
    }

    public BoardPosition(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public BoardPosition addRow(int delta) {
        return addRowCol(delta, 0);
    }

    public BoardPosition addCol(int delta) {
        return addRowCol(0, delta);
    }

    public BoardPosition addRowCol(int rowDelta, int colDelta) {
        return new BoardPosition(getRow() + rowDelta, getCol() + colDelta);
    }

    public boolean isOnBoard(int rowDelta, int colDelta) {
        int newRow = this.getRow() + rowDelta;
        int newCol = this.getCol() + colDelta;
        return newRow <= 7 && newRow >= 0 && newCol <= 7 && newCol >= 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BoardPosition that = (BoardPosition) o;

        if (getRow() != that.getRow()) {
            return false;
        }
        return getCol() == that.getCol();
    }

    @Override
    public int hashCode() {
        int result = getRow();
        result = 31 * result + getCol();
        return result;
    }
}
