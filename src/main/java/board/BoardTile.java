package board;

import board.figures.Figure;
import json.models.TileModel;

public class BoardTile {

    private Figure figure;

    private Side side;

    private boolean availableForMove;

    private boolean underAttack;

    public BoardTile(Figure figure, Side side) {
        this.figure = figure;
        this.side = side;
        this.availableForMove = false;
        this.underAttack = false;
    }

    public Figure getFigure() {
        return figure;
    }

    public Side getSide() {
        return side;
    }

    public void setFigure(Figure figure) {
        this.figure = figure;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    public boolean isAvailableForMove() {
        return availableForMove;
    }

    public void setAvailableForMove(boolean availableForMove) {
        this.availableForMove = availableForMove;
    }

    public boolean isUnderAttack() {
        return underAttack;
    }

    public void setUnderAttack(boolean underAttack) {
        this.underAttack = underAttack;
    }

    public TileModel toModel() {
        TileModel tileModel = new TileModel();
        tileModel.availableForMove = isAvailableForMove();
        tileModel.underAttack = isUnderAttack();
        tileModel.side = getSide();
        tileModel.figure = getFigure() == null ? null : getFigure().toModel();
        return tileModel;
    }
}
