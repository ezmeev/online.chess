package json.models;

import board.Side;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FigureModel {

    @JsonProperty("idx")
    public int idx;

    @JsonProperty("side")
    public Side side;

    @JsonProperty("type")
    public String type; // TODO TO ENUM!

    @JsonProperty("position")
    public BoardPositionModel position;
}
