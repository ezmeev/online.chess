package server.message.utils;

public class ByteUtils {

    private ByteUtils() {

    }

    public static OpCode toOpCode(byte b1) {
        byte rsv = (byte) ((b1 & ~(byte) 128) >> 4);
        return OpCode.getOpCode((byte) (b1 & 15));
    }

    public static byte toLength(byte b2) {

        return (byte) (b2 & ~(byte) 128);
    }

    public static boolean masked(byte b2) {
        return (b2 & -128) != 0;
    }
}
