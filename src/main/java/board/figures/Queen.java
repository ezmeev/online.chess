package board.figures;

import board.Board;
import board.BoardPosition;
import board.FigureMove;
import board.Side;

import java.util.ArrayList;
import java.util.List;

public class Queen extends Figure {
    public Queen(int idx, Side side, BoardPosition position) {
        super(idx, side, position);
    }

    @Override
    public List<FigureMove> getPossibleMoves(Board board) {
        BoardPosition position = getPosition();

        List<FigureMove> moves = new ArrayList<>();

        considerDirection(board, moves, position, 1, 1);
        considerDirection(board, moves, position, 1, -1);
        considerDirection(board, moves, position, -1, 1);
        considerDirection(board, moves, position, -1, -1);

        considerDirection(board, moves, position, 1, 0);
        considerDirection(board, moves, position, -1, 0);
        considerDirection(board, moves, position, 0, 1);
        considerDirection(board, moves, position, 0, -1);

        return moves;
    }

    @Override
    public List<FigureMove> getPossibleAttackMoves(Board board) {
        return getPossibleMoves(board);
    }
}
