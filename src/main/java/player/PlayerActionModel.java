package player;

public class PlayerActionModel {

    public String playerId;

    public PlayerActions action;

    public String payload;

    public PlayerActionModel(String playerId, PlayerActions action, String payload) {
        this.playerId = playerId;
        this.action = action;
        this.payload = payload;
    }
}
