package server;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import player.PlayerActionModel;
import player.PlayerActions;
import server.message.models.Message;
import server.message.models.MessageType;
import server.message.models.TextMessage;
import server.message.services.MessageReader;
import server.message.services.MessageWriter;
import server.message.utils.WebSocketHandshaker;

import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;

public class PlayerSocketHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerSocketHandler.class);

    private Socket socket;
    private MessageReader messageReader;
    private MessageWriter messageWriter;
    private Queue<PlayerActionModel> playersActionQueue;
    private ObjectMapper jsonSerializer = new ObjectMapper();
    private CompletableFuture<Void> listener;
    private final String sessionId;

    public PlayerSocketHandler(Socket socket, Queue<PlayerActionModel> playersActionQueue) throws IOException,
            NoSuchAlgorithmException {
        this.socket = socket;
        this.playersActionQueue = playersActionQueue;
        this.messageReader = new MessageReader(socket.getInputStream());
        this.messageWriter = new MessageWriter(socket.getOutputStream());
        this.sessionId = init();

        listener = CompletableFuture.runAsync(() -> {
            while (!socket.isClosed()) {
                checkClientUpdates();
            }
        });

        this.playersActionQueue.add(new PlayerActionModel(this.sessionId, PlayerActions.Join, this.sessionId));
    }

    public String getSessionId() {
        return sessionId;
    }

    public void send(TextMessage textMessage) {
        if (socket.isClosed()) {
            return; // TODO exclude this player from players list
        }
        try {
            messageWriter.writeMessage(textMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String init() throws NoSuchAlgorithmException, IOException {
        new WebSocketHandshaker(socket).handshake();
        Message initMessage = messageReader.readMessage();
        if (initMessage.getType() != MessageType.Text) {
            throw new IllegalStateException("Wrong init message type: [ " + initMessage.getType() + "]");
        }
        messageWriter.writeMessage(new TextMessage("{\"status\": \"ok\"}"));
        return initMessage.getPayload();
    }

    private void checkClientUpdates() {
        try {
            Message incomingMessage = messageReader.readMessage();
            if (incomingMessage.getType() == MessageType.Closing) {
                LOGGER.info("{} disconnected", sessionId);
                playersActionQueue.add(new PlayerActionModel(this.sessionId, PlayerActions.Disconnect, this.sessionId));
                socket.close();
            } else if (incomingMessage.getType() == MessageType.Text) {
                LOGGER.info("Payload: {}", incomingMessage.getPayload());
                playersActionQueue.add(deserializeToPlayerActionModel(incomingMessage));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PlayerActionModel deserializeToPlayerActionModel(Message incomingMessage) throws IOException {
        JsonNode json = jsonSerializer.readValue(incomingMessage.getPayload(), JsonNode.class);
        String action = json.get("action").asText();
        String payload = json.get("payload").toString();
        return new PlayerActionModel(this.sessionId, PlayerActions.valueOf(action), payload);
    }
}
