package engine.systems;

import board.GameState;
import engine.ChessEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.ChessPlayers;

public class ClientSynchronizationSystem implements EngineSystem {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientSynchronizationSystem.class);

    private GameStateSystem gameStateSystem;
    private PlayersSystem playersSystem;
    private String lastSentGameStateSnapshot = "";
    private String lastSentPlayersSnapshot = "";

    @Override
    public void addToEngine(int priority, ChessEngine chessEngine) {
        chessEngine.addSystem(priority, this);

        gameStateSystem = chessEngine.getSystem(GameStateSystem.class);
        playersSystem = chessEngine.getSystem(PlayersSystem.class);
    }

    @Override
    public void update() {
        ChessPlayers players = playersSystem.getPlayers();
        if (players == null) {
            return;
        }

        GameState gameState = gameStateSystem.getState();
        if (gameState == null) {
            return;
        }

        String gameStateSnapshot = gameState.getSnapshot();
        String playersSnapshot = players.getSnapshot();

        if (gameStateSnapshot.equals(lastSentGameStateSnapshot) && playersSnapshot.equals(lastSentPlayersSnapshot)) {
            return;
        }

        lastSentGameStateSnapshot = gameStateSnapshot;
        lastSentPlayersSnapshot = playersSnapshot;
        players.sendGameStateUpdate(gameState.toModel());
    }

}
