package player.actions;

import board.BoardPosition;
import json.models.SelectFigureActionModel;

public class SelectFigureAction {

    public int figureIdx;

    public BoardPosition position;

    public SelectFigureAction(SelectFigureActionModel actionModel) {
        figureIdx = actionModel.figureIdx;
        position = new BoardPosition(actionModel.position);
    }
}
