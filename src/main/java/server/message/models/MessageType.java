package server.message.models;


public enum MessageType {
    Closing,
    Text,
    Ping,
    Pong,
    Binary,
    Continuous
}