package json.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BoardPositionModel {

    @JsonProperty
    public int row;

    @JsonProperty
    public int col;
}
