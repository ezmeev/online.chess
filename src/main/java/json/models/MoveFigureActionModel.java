package json.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MoveFigureActionModel {
    @JsonProperty
    public int figureIdx;

    @JsonProperty
    public BoardPositionModel fromPosition;

    @JsonProperty
    public BoardPositionModel toPosition;
}
