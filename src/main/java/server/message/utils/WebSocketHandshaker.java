package server.message.utils;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebSocketHandshaker {

    private Socket socket;

    public WebSocketHandshaker(Socket socket) throws NoSuchAlgorithmException {
        this.socket = socket;
    }

    private String httpRequestDelimiter = "\\r\\n\\r\\n";
    private Pattern secWebSocketKeyPattern = Pattern.compile("Sec-WebSocket-Key: (.*)");
    private MessageDigest sha1 = MessageDigest.getInstance("SHA-1");

    public void handshake() throws IOException {
        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream = socket.getOutputStream();
        Scanner scanner = new Scanner(inputStream, "UTF-8").useDelimiter(httpRequestDelimiter);
        byte[] response = createHandshakeResponse(scanner.next());
        outputStream.write(response, 0, response.length);
    }

    private byte[] createHandshakeResponse(String handshakeRequest) {
        Matcher get = Pattern.compile("^GET").matcher(handshakeRequest);
        if (get.find()) {
            Matcher match = secWebSocketKeyPattern.matcher(handshakeRequest);
            match.find();
            String webSocketKey = match.group(1) + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
            return toByteArray("HTTP/1.1 101 Switching Protocols\r\n"
                    + "Connection: Upgrade\r\n"
                    + "Upgrade: websocket\r\n"
                    + "Sec-WebSocket-Accept:" + encodeResponse(webSocketKey) + "\r\n\r\n");
        } else {
            throw new IllegalArgumentException("Malformed handshake request: [$handshakeRequest]");
        }
    }

    private String encodeResponse(String webSocketKey) {
        return DatatypeConverter.printBase64Binary(sha1.digest(toByteArray(webSocketKey)));
    }

    private byte[] toByteArray(String string) {
        return string.getBytes(StandardCharsets.UTF_8);
    }
}