package server.message.utils;

import java.util.Arrays;

public enum OpCode {

    CONTINUOUS(0),
    TEXT(1),
    BINARY(2),
    PING(8),
    PONG(9),
    CLOSING(10);

    public int code;

    private OpCode(int code) {
        this.code = code;
    }

    public static OpCode getOpCode(int code) {
        return Arrays.stream(OpCode.values()).filter(o -> o.code == code).findFirst().orElse(null);
    }
}
