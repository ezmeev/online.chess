package board.figures;

import board.Board;
import board.BoardPosition;
import board.FigureMove;
import board.Side;
import json.models.BoardPositionModel;
import json.models.FigureModel;

import java.util.List;

public abstract class Figure {

    private Side side;

    private int idx;

    private BoardPosition position;

    public Figure(int idx, Side side, BoardPosition position) {
        this.idx = idx;
        this.side = side;
        this.position = position;
    }

    public boolean isMoveAllowed(Board board, FigureMove move) {
        return getPossibleMoves(board).contains(move);
    }

    public abstract List<FigureMove> getPossibleMoves(Board board);

    public abstract List<FigureMove> getPossibleAttackMoves(Board board);

    public Side getSide() {
        return this.side;
    }

    public String getType() {
        return getClass().getSimpleName();
    }

    public int getIdx() {
        return idx;
    }

    public BoardPosition getPosition() {
        return position;
    }

    public void setPosition(BoardPosition position) {
        this.position = position;
    }

    protected boolean enemyTile(Board board, BoardPosition position) {
        Figure figure = board.getFigure(position);
        return figure != null && figure.getSide() != this.getSide();
    }

    protected boolean emptyTile(Board board, BoardPosition position) {
        return board.getFigure(position) == null;
    }

    protected void tryAdd(Board board, List<FigureMove> moves, BoardPosition position, int addRow, int addCol) {
        if (position.isOnBoard(addRow, addCol)) {
            boolean tileIsEmpty = emptyTile(board, position.addRowCol(addRow, addCol));
            boolean tileIsEnemy = enemyTile(board, position.addRowCol(addRow, addCol));
            if (tileIsEmpty || tileIsEnemy) {
                moves.add(new FigureMove(position, position.addRowCol(addRow, addCol), tileIsEnemy));
            }
        }
    }

    protected void considerDirection(Board board, List<FigureMove> moves, BoardPosition position, int rowDirection, int
            colDirection) {
        int i = 1;
        while (i <= 7) {
            if (!position.isOnBoard(i * rowDirection, i * colDirection)) {
                break;
            }

            if (emptyTile(board, position.addRowCol(i * rowDirection, i * colDirection))) {
                tryAdd(board, moves, position, i * rowDirection, i * colDirection);
            } else if (enemyTile(board, position.addRowCol(i * rowDirection, i * colDirection))) {
                tryAdd(board, moves, position, i * rowDirection, i * colDirection);
                break;
            } else {
                break;
            }
            i++;
        }
    }

    public FigureModel toModel() {
        FigureModel model = new FigureModel();
        model.idx = getIdx();
        model.side = getSide();
        model.type = getType();
        model.position = new BoardPositionModel();
        model.position.row = getPosition().getRow();
        model.position.col = getPosition().getCol();
        return model;
    }
}
