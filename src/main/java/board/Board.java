package board;

import board.figures.Figure;
import json.models.BoardModel;
import json.models.TileModel;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Board {

    private BoardTile[][] tiles = null;

    public Board() {
        tiles = BoardHelpers.setupTiles();
    }

    public Figure getFigure(BoardPosition position) {
        return tiles[position.getRow()][position.getCol()].getFigure();
    }

    public BoardTile[][] getTiles() {
        return tiles;
    }

    public boolean isPositionAttackedBySide(Side side, BoardPosition position) {
        return Arrays.stream(getTiles())
                .flatMap(Arrays::stream)
                .map(BoardTile::getFigure)
                .filter(Objects::nonNull)
                .filter(f1 -> f1.getSide() == side)
                .flatMap(f -> f.getPossibleAttackMoves(this).stream())
                .anyMatch(m -> m.getTo().equals(position));
    }

    public BoardModel toModel() {
        TileModel[][] tilesModel = new TileModel[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                tilesModel[i][j] = getTiles()[i][j].toModel();
            }
        }
        BoardModel model = new BoardModel();
        model.tiles = tilesModel;
        return model;
    }

    public void applyMoveCommand(MoveCommand command) {

        BoardPosition figureToDiePosition = command.getFigureToDiePosition();
        BoardPosition figureToMovePosition = command.getFigureToMovePosition();
        Figure figureToMove = command.getFigureToMove();
        Figure figureToDie = command.getFigureToDie();

        figureToMove.setPosition(figureToDiePosition);

        if (figureToDie != null) {
            figureToDie.setPosition(null);
        }

        getTiles()[figureToMovePosition.getRow()][figureToMovePosition.getCol()].setFigure(null);
        getTiles()[figureToDiePosition.getRow()][figureToDiePosition.getCol()].setFigure(figureToMove);
    }

    public void undoMoveCommand(MoveCommand command) {

        BoardPosition figureToDiePosition = command.getFigureToDiePosition();
        BoardPosition figureToMovePosition = command.getFigureToMovePosition();
        Figure figureToMove = command.getFigureToMove();
        Figure figureToDie = command.getFigureToDie();

        figureToMove.setPosition(figureToMovePosition);
        if (figureToDie != null) {
            figureToDie.setPosition(figureToDiePosition);
        }

        getTiles()[figureToMovePosition.getRow()][figureToMovePosition.getCol()].setFigure(figureToMove);
        getTiles()[figureToDiePosition.getRow()][figureToDiePosition.getCol()].setFigure(figureToDie);
    }

    @SuppressWarnings("unchecked")
    public <T> T findFirstFigureOfType(Side side, Class<T> figureClass) {
        return (T) Arrays.stream(getTiles())
                .flatMap(Arrays::stream)
                .map(BoardTile::getFigure)
                .filter(Objects::nonNull)
                .filter(f -> f.getSide() == side && f.getClass().equals(figureClass))
                .findFirst()
                .orElse(null);
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> findFiguresOfType(Class<T> figureClass) {
        return Arrays.stream(getTiles())
                .flatMap(Arrays::stream)
                .map(BoardTile::getFigure)
                .filter(Objects::nonNull)
                .filter(f -> f.getClass().equals(figureClass))
                .map(f -> (T) f)
                .collect(Collectors.toList());
    }
}
