package json.models;

import board.Side;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TileModel {

    @JsonProperty("figure")
    public FigureModel figure;

    @JsonProperty("side")
    public Side side;

    @JsonProperty("availableForMove")
    public boolean availableForMove;

    @JsonProperty("underAttack")
    public boolean underAttack;
}
