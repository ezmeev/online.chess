package engine;

import engine.systems.EngineSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ChessEngine {

    public static final Logger LOGGER = LoggerFactory.getLogger(ChessEngine.class);

    private List<EngineSystem> systems = new ArrayList<>();
    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private boolean started = false;

    public void addSystem(int priority, EngineSystem system) {
        systems.add(priority - 1, system);
    }

    @SuppressWarnings("unchecked")
    public <T extends EngineSystem> T getSystem(Class<T> systemType) {
        return (T) systems
                .stream()
                .filter(s -> s.getClass().equals(systemType))
                .findFirst().orElse(null);
    }

    public void start() {
        LOGGER.info("Starting engine ...");

        if (started) {
            return;
        }
        started = true;
        executor.scheduleAtFixedRate(this::tick, 0L, 100L, TimeUnit.MILLISECONDS);
    }

    private void tick() {
        try {
            for (EngineSystem system : systems) {
                system.update();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
